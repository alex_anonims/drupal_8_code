<?php

/**
 * @file
 * File contains butch operation to update nodes.
 */

/**
 * Operation to update node 'nid' field.
 *
 * @param array $items
 *   Array of node ids.
 * @param array $context
 *   Array with butch context information.
 */
function node_update(array $items, array &$context) {
  $limit = 50;

  if (empty($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($items);
  }

  if (empty($context['sandbox']['items'])) {
    $context['sandbox']['items'] = $items;
  }

  $counter = 0;
  if (!empty($context['sandbox']['items'])) {
    if ($context['sandbox']['progress'] != 0) {
      array_splice($context['sandbox']['items'], 0, $limit);
    }

    foreach ($context['sandbox']['items'] as $nid) {
      if ($counter != $limit) {
        $node_storage = \Drupal::entityTypeManager()->getStorage('node');
        $node = $node_storage->load($nid)
        $node->field_hidden_nid->setValue(['value' => $nid])->save();

        $counter++;
        $context['sandbox']['progress']++;

        $context['message'] = $this->t('Current node :progress', [
          ':progress' => $context['sandbox']['progress'],
        ]);

        $context['results']['processed'] = $context['sandbox']['progress'];
      }
    }
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}
