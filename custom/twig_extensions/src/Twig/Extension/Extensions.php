<?php

namespace Drupal\twig_extensions\Twig\Extension;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Twig_Extension;
use Twig_SimpleFilter;

/**
 * Custom twig extensions.
 */
class Extensions extends Twig_Extension {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * Constructor for Extensions class.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Constructor for Extensions class.
   *
   * @return array
   *   Array of Twig filters.
   */
  public function getFilters() {
    $filters = [];

    $filters[] = new Twig_SimpleFilter('child_id', [
      $this,
      'childId',
    ]);

    $filters[] = new Twig_SimpleFilter('parent_url', [
      $this,
      'parentUrl',
    ]);

    $filters[] = new Twig_SimpleFilter('parent_name', [
      $this,
      'parentName',
    ]);

    $filters[] = new Twig_SimpleFilter('remove_special', [
      $this,
      'removeSpecialChar',
    ]);

    $filters[] = new Twig_SimpleFilter('remove_h2', [
      $this,
      'removeH2',
    ]);

    $filters[] = new Twig_SimpleFilter('trim_title', [
      $this,
      'trimTitle',
    ]);

    return $filters;
  }

  /**
   * Returns parent url.
   *
   * @param string $id
   *   Term id.
   *
   * @return string
   *   Rendered URL to taxonomy term.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the 'taxonomy_term' entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the 'taxonomy_term' storage handler couldn't be loaded.
   */
  public function parentUrl($id) {
    $parentElement = $this->entityTypeManager->getStorage('taxonomy_term')->loadParents($id);
    if ($parentElement != NULL) {
      foreach ($parentElement as $value) {
        $tid = $value->id();
      }
      $options = ['absolute' => TRUE];
      $categoryUrl = new Url('entity.taxonomy_term.canonical', ['taxonomy_term' => $tid], $options);
      return $categoryUrl;
    }
    else {
      $options = ['absolute' => TRUE];
      $categoryUrl = new Url('entity.taxonomy_term.canonical', ['taxonomy_term' => $id], $options);
      return $categoryUrl;
    }
  }

  /**
   * Returns parent name.
   *
   * @param string $id
   *   Term id.
   *
   * @return string
   *   Taxonomy term label.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the 'taxonomy_term' entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the 'taxonomy_term' storage handler couldn't be loaded.
   */
  public function parentName($id) {
    $taxonomyStorage = $this->entityTypeManager->getStorage('taxonomy_term');
    $parentElement = $taxonomyStorage->loadParents($id);
    if ($parentElement != NULL) {
      foreach ($parentElement as $value) {
        $name = $value->getName();
      };
    }
    else {
      $name = $taxonomyStorage->load($id)->getName();
    }
    return $name;
  }

  /**
   * Removes special characters from node title.
   *
   * @param string $title
   *   String that should be filtered.
   *
   * @return string
   *   Filtered string.
   */
  public function removeSpecialChar($title) {
    $newString = str_replace("'", "", $title);
    $newString = preg_replace('/[^\p{L}\p{N}]/u', '_', $newString);
    return $newString;
  }

  /**
   * Removes substring between h2 tags.
   *
   * @param string $title
   *   String with H2 tag that should be filtered.
   *
   * @return string
   *   Filtered string.
   */
  public function removeH2($title) {
    $rendered = render($title);
    return strpos($rendered, '<h2>') !== NULL ? preg_replace('/<h2>[\s\S]+?<\/h2>/', '', $rendered) : $rendered;
  }

  /**
   * Trims titles.
   *
   * @param string $title
   *   String with H2 tag that should be filtered.
   *
   * @return string
   *   Trimmed title.
   */
  public function trimTitle($title) {
    $stripped = strip_tags($title);
    if (strlen($stripped) >= 65) {
      return substr($stripped, 0, strpos(wordwrap($stripped, '65'), "\n")) . '...';
    }
    else {
      return $stripped;
    }
  }

}
