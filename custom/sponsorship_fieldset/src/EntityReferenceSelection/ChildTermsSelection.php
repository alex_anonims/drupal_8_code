<?php

namespace Drupal\sponsorhip_fieldset\Plugin\EntityReferenceSelection;

use Drupal\taxonomy\Plugin\EntityReferenceSelection\TermSelection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides specific access control for the taxonomy_term entity type.
 *
 * @EntityReferenceSelection(
 *   id = "sponsor:taxonomy_term",
 *   label = @Translation("Taxonomy Term autocomplete selection"),
 *   base_plugin_label = @Translation("Taxonomy Term autocomplete selection"),
 *   entity_types = {"taxonomy_term"},
 *   group = "custom",
 *   weight = 2
 * )
 */
class ChildTermsSelection extends TermSelection implements ContainerFactoryPluginInterface {

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {

    $vocabularies = $this->getConfiguration()['target_bundles'];

    foreach ($vocabularies as $vocabulary) {
      $queryTerms = $this->connection->select('taxonomy_term_field_data', 't');
      $queryTerms->join('taxonomy_term__parent', 'tp', 'tp.entity_id = t.tid');
      $queryTerms->addField('t', 'name');
      $queryTerms->addField('t', 'tid');
      $queryTerms->condition('t.vid', $vocabulary);
      $queryTerms->condition('tp.parent_target_id', '0', 'NOT IN');
      $queryTerms->condition('t.name', '%' . $match . '%', 'LIKE');
      $resultTerms = $queryTerms->execute()->fetchAll();

      foreach ($resultTerms as $term) {
        $options[$vocabulary][$term->tid] = $term->name;
      }
    }

    return $options;
  }

}
