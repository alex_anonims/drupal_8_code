<?php

namespace Drupal\general\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\general\Services\NodesAndTermsRender;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides 'Recent articles' block.
 *
 * @Block(
 *   id = "recent_articles_parent",
 *   admin_label = @Translation("Recent articles"),
 * )
 */
class RecentArticles extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $route;

  /**
   * Render service.
   *
   * @var \Drupal\general\Services\NodesAndTermsRender
   */
  protected $renderService;

  /**
   * Creates an SponsorFilterField object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route
   *   The current route match service.
   * @param \Drupal\general\Services\NodesAndTermsRender $renderService
   *   The service to render items.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RouteMatchInterface $route,
    NodesAndTermsRender $renderService
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->route = $route;
    $this->renderService = $renderService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('general.render_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $termStorage = $this->renderService->entityTypeManager->getStorage('taxonomy_term');
    $termParameter = $this->route->getParameter('taxonomy_term');
    if ($termParameter && $termStorage->load($termParameter->id())->getVocabularyId() === 'primary_categories') {
      $tid = $termParameter->id();
      $parent_term = $termStorage->load($tid);
      $parentElement = $termStorage->loadParents($tid);
      if (empty($parentElement) && $parent_term->getVocabularyId() === 'primary_categories') {
        $service = $this->renderService;
        $render = $service->outputPrepare($tid);

        $block = [
          '#type' => 'markup',
          '#cache' =>
            ['max-age' => 0],
          '#markup' => $render,
        ];

        $destination = Link::createFromRoute(t('View More'), 'general.loadmore', [], [
          'query' => ['page' => 1, 'tid' => $tid],
          'attributes' => ['class' => 'use-ajax', 'id' => 'view-more-link'],
        ])->toString();
        if (count($render) > 5) {
          $block['button'] = $destination;
        }
        return $block;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
