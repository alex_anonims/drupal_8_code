<?php

namespace Drupal\general\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\BeforeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\general\Services\NodesAndTermsRender;

/**
 * Class to provide loadind nodes and taxonomy terms user click.
 */
class LoadmoreController extends ControllerBase {

  /**
   * Request service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Render service.
   *
   * @var \Drupal\general\Services\NodesAndTermsRender
   */
  protected $renderService;

  /**
   * Creates an LoadmoreController object.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The database connector.
   * @param \Drupal\general\Services\NodesAndTermsRender $renderService
   *   The service to render items.
   */
  public function __construct(
    Request $request,
    NodesAndTermsRender $renderService
  ) {
    $this->request = $request;
    $this->renderService = $renderService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('general.render_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function loadmore() {
    $page = $this->request->query->get('page');
    $tid = $this->request->query->get('tid');
    $service = $this->renderService;
    $render = $service->outputPrepare($tid, $page);
    $response = new AjaxResponse();
    for ($key = 0; $key < 6; $key++) {
      if ($key === 0 || $key === 5) {
        $response->addCommand(new BeforeCommand('#content-area', '<div class="col-md-6">' . $render[$key] . '</div>'));
      }
      else {
        $response->addCommand(new BeforeCommand('#content-area', '<div class="col-md-3">' . $render[$key] . '</div>'));
      }
    }

    $destination = Link::createFromRoute(t('View More'), 'general.loadmore', [], [
      'query' => [
        'page' => ++$page,
        'tid' => $tid,
      ],
      'attributes' => [
        'class' => 'use-ajax',
        'id' => 'view-more-link',
      ],
    ])->toString();
    $response->addCommand(new ReplaceCommand('#view-more-link', $destination));
    if (count($render) < 6) {
      $response->addCommand(new ReplaceCommand('#view-more-link', ''));
    }
    return $response;
  }

}
